<?php

namespace Drupal\form_json\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Form json plugin mannager item annotation object.
 *
 * @see \Drupal\form_json\Plugin\FormJsonPluginMannagerManager
 * @see plugin_api
 *
 * @Annotation
 */
class FormJsonPluginMannager extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Setting if Enabled.
   *
   * @var \Drupal\Core\Annotation\enabled
   */
  public $enabled;

}
