<?php

namespace Drupal\form_json\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\form_json\Plugin\FormJsonPluginMannagerManager;

/**
 * Class AdminFormJson.
 */
class AdminFormJson extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Drupal\form_json\Plugin\FormJsonPluginMannagerManager definition.
   *
   * @var \Drupal\form_json\Plugin\FormJsonPluginMannagerManager
   */
  protected $pluginManagerFormJsonPluginMannager;
  /**
   * Constructs a new AdminFormJson object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      ConfigManager $config_manager,
    FormJsonPluginMannagerManager $plugin_manager_form_json_plugin_mannager
    ) {
    parent::__construct($config_factory);
        $this->configManager = $config_manager;
    $this->pluginManagerFormJsonPluginMannager = $plugin_manager_form_json_plugin_mannager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('config.manager'),
      $container->get('plugin.manager.form_json_plugin_mannager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'form_json.adminformjson',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_form_json';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('form_json.adminformjson');

    $options = [
      'none' => $this->t('none'),
    ];
    $type_manager = $this->pluginManagerFormJsonPluginMannager;
    $plugin_definitions = $type_manager->getDefinitions();
    foreach ($plugin_definitions as $plugin_id => $plugin) {
      $options[$plugin_id] = $this->t('@id', ['@id' => $plugin_id]);
    }
    $form['enable_plugin'] = [
      '#type' => 'radios',
      '#title' => $this->t('Enable Plugin'),
      '#description' => $this->t('Select the translator plugin to alter the default output schema'),
      '#options' => $options,
      '#default_value' => $config->get('enable_plugin'),
    ];

    $form['extra'] = [
      '#markup' => '<p> Put helpfull info here test /api/form/node/article or api/form/node/article/1</p>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('form_json.adminformjson')
      ->set('enable_plugin', $form_state->getValue('enable_plugin'))
      ->save();
  }

}
