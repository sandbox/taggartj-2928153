<?php

namespace Drupal\form_json\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Form json plugin mannager plugin manager.
 */
class FormJsonPluginMannagerManager extends DefaultPluginManager {

  public $maps;
  /**
   * Constructs a new FormJsonPluginMannagerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/FormJsonPluginMannager', $namespaces, $module_handler, 'Drupal\form_json\Plugin\FormJsonPluginMannagerInterface', 'Drupal\form_json\Annotation\FormJsonPluginMannager');

    $this->alterInfo('form_json_form_json_plugin_mannager_info');
    $this->setCacheBackend($cache_backend, 'form_json_form_json_plugin_mannager_plugins');
  }

}
