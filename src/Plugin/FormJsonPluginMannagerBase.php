<?php

namespace Drupal\form_json\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Form json plugin mannager plugins.
 */
abstract class FormJsonPluginMannagerBase extends PluginBase implements FormJsonPluginMannagerInterface {

  // Add common methods and abstract methods for your plugin type here.
  abstract function translateFormSchema(array $data, $entity);

}
