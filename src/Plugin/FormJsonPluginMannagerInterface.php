<?php

namespace Drupal\form_json\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Form json plugin mannager plugins.
 */
interface FormJsonPluginMannagerInterface extends PluginInspectionInterface {

  // Add get/set methods for your plugin type here.
  function translateFormSchema(array $data, $entity);

}
