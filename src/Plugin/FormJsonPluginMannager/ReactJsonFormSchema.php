<?php

namespace Drupal\form_json\Plugin\FormJsonPluginMannager;

use Drupal\form_json\Annotation\FormJsonPluginMannager;
use Drupal\form_json\Plugin\FormJsonPluginMannagerBase;

/**
 * Provides a 'json' data source.
 *
 * @FormJsonPluginMannager(
 *   id = "react-jsonschema-form",
 *   label = @Translation("react-jsonschema-form"),
 *   enabled = "TRUE",
 * )
 */
class ReactJsonFormSchema extends FormJsonPluginMannagerBase {
  public function getName() {
    return 'react-jsonschema-form';
  }

  public function translateFormSchema(array $data, $entity) {
    $drupal_fields = [
      'comment' => ['na'],
      'datetime' => [
        'properties' => [
          'type' => 'string',
          'format' =>'date',
        ],
      ],
      'file' => [
        'properties' => [
          'type' => 'string',
          'format' =>'data-url',
        ],
      ],
      'image' => [
        'properties' => [
          'type' => 'string',
          'format' =>'data-url',
        ],
      ],
      'link',
      'list_float' => [
        'properties' => [
          'type' => 'array',
          'items' => [

          ],
        ],
      ],
      'list_integer' => [
        'properties' => [
          'type' => 'array',
          'items' => [
          ],
        ],
      ],
      'list_string' => [
        'properties' => [
          'type' => 'array',
          'items' => [
          ],
        ],
      ],
      'path' => [
        'properties' => [
          'type' => 'string',
        ],
      ],
      'text' => [
        'properties' => [
          'type' => 'textarea',
        ],
      ],
      'text_long' => [
        'properties' => [
          'type' => 'textarea',
        ],
      ],
      'text_with_summary' => [
        'properties' => [
          'type' => 'textarea',
        ],
      ],
      'boolean' => [
        'properties' => [
          'type' => 'boolean',
        ],
      ],
      'changed' => [
        'properties' => [
          'type' => 'integer',
        ],
      ],
      'created' => [
        'properties' => [
          'type' => 'integer',
        ],
      ],
      'decimal'  => [
        'properties' => [
          'type' => 'number',
        ],
      ],
      'email' => [
        'properties' => [
          'type' => 'string',
          'format' =>'email',
        ],
      ],
      'entity_reference' => [
        'properties' => [
          'type' => 'array',
        ],
      ],
      'float' => [
        'properties' => [
          'type' => 'number',
        ],
      ],
      'integer' => [
        'properties' => [
          'type' => 'integer',
        ],
      ],
      'language' => ['na'],
      'map' => ['na'],
      'password'  => [
        'properties' => [
          'type' => 'string',
        ],
      ],
      'string' => [
        'properties' => [
          'type' => 'string',
        ],
      ],
      'string_long' => [
        'properties' => [
          'type' => 'textarea',
        ],
      ],
      'timestamp'=> [
        'properties' => [
          'type' => 'integer',
        ],
      ],
      'uri' => [
        'properties' => [
          'type' => 'string',
          'format' =>  'uri',
        ],
      ],
      'uuid' => ['na'],
    ];
    $fields = $data['form']['fields'];
    $field_types_mapped = [];
    $formData = [];
    // Go through the default data and map the above array.
    foreach ($fields as $field_name => $field_values) {
      if (!empty($field_values['field_type'])) {
        $type = $field_values['field_type'];
        $mapped =  $drupal_fields[$type];
        if (!empty($mapped['properties'])) {
          $mapped_type = $mapped['properties']['type'];
          $field_types_mapped[$field_name] = $mapped['properties'];
          // Title.
          $field_types_mapped[$field_name]['title'] = $field_values['label'];
          // Description.
          if (!empty($field_values['description'])) {
            $field_types_mapped[$field_name]['description'] = $field_values['description'];
          }

          // @TODO move Setting default values to some common function.
          // Default values if editing an entity.
          if (!empty($field_values['#default'])) {
            if (is_array($field_values['#default'])) {
              // Stinging types.
              if (in_array($mapped_type, ['string', 'textarea']) && !empty($field_values['#default']['value'])) {
                // This should populate the form data.
                $formData[$field_name] = $field_values['#default']['value'];
              }
              // Boolean types.
              if ($mapped_type == 'boolean' && !empty($field_values['#default']['value'])) {
                $temp_val = FALSE;
                if ($field_values['#default']['value'] == 1) {
                  $temp_val = TRUE;
                }
                $field_types_mapped[$field_name]['default'] = $temp_val;
                $formData[$field_name] = $temp_val;
              }
              // @TODO Options.
              // @TODO entity reff.
            }
          }

          // Non Default aka new post request.
          if (!empty($field_values['default_value'])) {
            if ($mapped_type == 'boolean') {
              $field_types_mapped[$field_name]['default'] = $field_values['default_value']['value'];
            }
          }
        }
      }
    }
    $form = [
      'JSONSchema' => $field_types_mapped,
      'formData' => $formData,
      'original_data' => $data,
    ];
    return $form;
  }

  /*
   * Helper function to get all drupal fields.
  public function getFieldTypes() {
    $defs = \Drupal::service('plugin.manager.field.field_type')->getDefinitions();
    foreach ($defs as $key => $value ) {
      $keys[] = $key;
    }
    return $keys;
  }
  */


}
