<?php

namespace Drupal\form_json\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Serialization\Json;
use Drupal\serialization\Encoder\JsonEncoder;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\jsonapi\EntityToJsonApi;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;
use Drupal\form_json\Plugin\FormJsonPluginMannagerManager;
use Drupal\Core\Config\ConfigManager;

/**
 * Class FormToJsonApiController.
 */
class FormToJsonApiController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Component\Serialization\Json definition.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $serializationJson;

  /**
   * Drupal\serialization\Encoder\JsonEncoder definition.
   *
   * @var \Drupal\serialization\Encoder\JsonEncoder
   */
  protected $serializerEncoderJson;

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Jason Api EntityToJsonApi
   *
   * @var EntityToJsonApi
   */
  protected $entityToJsonApi;

  /**
   * Jason form FormJsonPluginMannagerInterface
   *
   * @var FormJsonPluginMannager
   */
  protected $formJsonPlugInMannager;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Constructs a new FormToJsonApiController object.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    RequestStack $request_stack,
    Json $serialization_json,
    JsonEncoder $serializer_encoder_json,
    FormBuilder $form_builder,
    EntityToJsonApi $entity_to_json,
    FormJsonPluginMannagerManager $form_json_plugin_manager,
    ConfigManager $config_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->serializationJson = $serialization_json;
    $this->serializerEncoderJson = $serializer_encoder_json;
    $this->formBuilder = $form_builder;
    $this->entityToJsonApi = $entity_to_json;
    $this->formJsonPlugInMannager = $form_json_plugin_manager;
    $this->configManager = $config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('serialization.json'),
      $container->get('serializer.encoder.json'),
      $container->get('form_builder'),
      $container->get('jsonapi.entity.to_jsonapi'),
      $container->get('plugin.manager.form_json_plugin_mannager'),
      $container->get('config.manager')
    );
  }

  /**
   * Handle form request.
   *
   * @return JsonResponse
   *  Returns a JsonResponse object.
   */
  public function handleFormRequest($entity, $bundal, $id = NULL) {
    $status = 200;
    // @todo handle add / edit
    if (empty($id)) {
      $mode = 'POST';
      $entity_object = $this->getEntity($entity, $bundal);
    }
    else {
      $mode = 'PUT';
      $entity_object = $this->getEntity($entity, $bundal, $id);
    }
    if ($entity_object instanceof JsonResponse) {
      return $entity_object;
    }
    // Ok we have an entity lets check if it's ok.
    $implements = class_implements($entity_object);
    if (is_array($implements)) {
      if (!array_key_exists('Drupal\Core\Entity\FieldableEntityInterface', $implements)) {
        return $this->returnJsonError('Entity does not implement FieldableEntityInterface');
      }
    }
    $normalized = $this->entityToJsonApi->normalize($entity_object);
    // Yep ok it does so we good for now.
    $all_fields = $entity_object->getFieldDefinitions();
    $cleaned_fields = [];

    $to_unset = [
      'nid',
      'tid',
      'uuid',
      'vid',
      'langcode',
      'type',
      'uid',
      'created',
      'changed',
      'revision_timestamp',
      'revision_uid',
      'path',
      'comment',
      'revision_translation_affected',
      'default_langcode',
    ];

    $form = $this->getForm($entity, $entity_object, $mode);
    // Check for error.
    if ($form instanceof JsonResponse) {
      return $form;
    }
    $imp = class_implements($form);
    if (is_array($imp)) {
      if (!array_key_exists('Drupal\Core\Form\FormInterface', $imp)) {
        return $this->returnJsonError('Form not valid to export');
      }
    }
    else {
      // Error here.
      return $this->returnJsonError('Form not found or valid');
    }
    // Build the form.
    $form_build = $this->formBuilder->getForm($form);

    // @TODO make hook for other modules to change unset or add.
    if (is_array($all_fields)) {
      foreach ($all_fields as $field_name => $field_object) {
        if (!in_array($field_name, $to_unset)) {
          $field_renderd = $field_object->toArray();
          // Unset stuff we dont need here.
          if (!empty($field_renderd['label']) && is_object($field_renderd['label'])) {
            $field_renderd['label'] = $field_renderd['label']->__toString();
          }
          if (!empty($field_renderd['description']) && is_object($field_renderd['description'])) {
            $field_renderd['description'] = $field_renderd['description']->__toString();
          }
          if (!empty($field_renderd['default_value'][0])) {
            $field_renderd['default_value'] = $field_renderd['default_value'][0];
          }

          unset($field_renderd['entity_type']);
          unset($field_renderd['bundle']);
          unset($field_renderd['uuid']);
          unset($field_renderd['_core']);
          unset($field_renderd['revisionable']);
          unset($field_renderd['translatable']);
          // All ways add a 'field_type'.
          if (empty($field_renderd['field_type'])) {
            $field_renderd['field_type'] = $field_object->getType();
          }
          if (!empty($form_build[$field_name])) {
            //dump($form_build[$field_name]);
            $field_renderd['id'] = $form_build[$field_name]['#id'];
            $field_renderd['weight'] = $form_build[$field_name]['#weight'];
          }

          if (!empty($id)) {
            $default_value = $this->handleDefaultValue($field_name, $entity_object);
            $field_renderd['#default'] = $default_value;
          }
          $cleaned_fields[$field_name] = $field_renderd;
        }
      }
    }

    $normalized['fields'] = $cleaned_fields;

    // @TODO make a hook for other modules to unset or add data here.
    $data = [
      'paramaters' => [
      'entity' => $entity,
      'bundal' => $bundal,
      'mode' => $mode
      ],
      'form' => $normalized,
    ];
    // Translate this array in to the what ever skema required specific.
    $config = $this->config('form_json.adminformjson');
    $enable = $config->get('enable_plugin');
    if (!empty($config->get('enable_plugin')) && $config->get('enable_plugin') != 'none') {
      $type_manager = $this->formJsonPlugInMannager;
      $plugin_definitions = $type_manager->getDefinitions();
      foreach ($plugin_definitions as $plugin_id => $plugin) {
        $instance = $type_manager->createInstance($plugin_id);
        if ($plugin_id === $enable ) {
          $data = $instance->translateFormSchema($data, $entity_object);
        }
      }
    }

    return new JsonResponse($data, $status);
  }

  /**
   * @param $entity
   * @param $bundal
   * @param null $id
   * @return mixed JsonResponse|Array
   */
  protected function getEntity($entity, $bundal, $id = NULL) {
    if ($entity != 'node') {
      return $this->returnJsonError('We only Support Nodes at the moment');
    }
    if ($bundal == 'none') {
      $paramaters = [];
    }
    else {
      $paramaters = [
        'type' => $bundal,
      ];
    }
    // we have an id.
    if (!empty($id)) {
      $load_entity = $this->entityTypeManager->getStorage($entity)->load($id);
    }
    else {
      $load_entity = $this->entityTypeManager->getStorage($entity)->create($paramaters);
    }

    if (is_object($load_entity)) {
      return $load_entity;
    }
    else {
      return $this->returnJsonError('Entity not valid' . $entity . '/'. $bundal);
    }
  }

  /**
   * This Is to get an entity Form Mode object example NodeForm.
   *
   * @param $entity
   * @param $mode
   * @return JsonResponse
   *  Returns a JsonResponse object.
   */
  protected function getForm($entity, $loaded_entity, $mode) {
    if ($entity != 'node') {
      return $this->returnJsonError('We only Support Nodes at the moment');
    }
    // @todo open to form modes 'default' vs 'custom_form_mode'
    $form = $this->entityTypeManager->getFormObject($entity, 'default')->setEntity($loaded_entity);
    if (is_object($form)) {
      return $form;
    }
    else {
      return $this->returnJsonError('Form Not found for ' . $entity);
    }
  }

  /**
   * @param $field_name
   * @param $entity
   * @return array
   */
  public function handleDefaultValue($field_name, $entity) {
    $value = [];
    $value_object = $entity->get($field_name);
    if ($value_object instanceof FieldItemList) {
      if (!empty($value_object->getValue()[0]['value'])) {
        $value['value'] = $value_object->getValue()[0]['value'];
      }
    }
    if ($value_object instanceof EntityReferenceFieldItemList) {
      $value['default'] = $value_object->getValue();
      if (count($value['default']) != 0) {
        $loaded_entities = $value_object->referencedEntities();
        foreach ($loaded_entities as $key => $entity_ref) {
          // Find out what type.
          if ($entity_ref instanceof File) {
            $url = $entity_ref->url();
            $file_data = [
              'filename' => $entity_ref->filename->getValue()[0]['value'],
              'filesize' => $entity_ref->filesize->getValue()[0]['value'],
              'fid' => $entity_ref->id(),
              'url' => $url,
            ];
            $value['entity_value'][$key] = $file_data;
          }

          if ($entity_ref instanceof Term) {
            $term_data = [
              'Name' => $entity_ref->name->getValue()[0]['value'],
              'tid' => $entity_ref->tid->getValue()[0]['value'],
            ];
            $value['entity_value'][$key] = $term_data;
          }
        }
      }
    }
    return $value;
  }

  /**
   * This is to set a Error responce.
   *
   * @param mixed $message
   *   This can be an array or string.
   * @param null $status
   *   Optional default status is 400.
   *
   * @return JsonResponse
   *   Return a new JsonResponse object.
   */
  public function returnJsonError($message, $status = NULL) {
    if (empty($status)) {
      $status = 400;
    }
    if (is_string($message)) {
      $message = [
        'error' => $message,
      ];
    }
    return new JsonResponse($message, $status);
  }

}
